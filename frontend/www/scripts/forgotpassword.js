async function sendResetMail(event) {
    let form = document.querySelector("#form-forgot-password");
    let formData = new FormData(form);
    await sendRequest("POST", `${HOST}/api/password/reset/`, formData, "");
    window.location.replace("resetconfirmed.html");
  }

document.querySelector("#btn-reset-password").addEventListener("click", async (event) => await sendResetMail(event));
