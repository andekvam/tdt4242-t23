async function getAthletes() {
    let currentUser = await getCurrentUser();
    let athleteSelect = document.querySelector("select");
    for(let athleteUrl of currentUser.athletes){
        let response = await sendRequest("GET", athleteUrl);
        let athlete = await response.json();
        let option = document.createElement("option");
        option.value = athleteUrl;
        option.innerText = athlete.username;
        athleteSelect.append(option);
    }
}
async function getData(athleteUrl){
    if(athleteUrl.length > 0){
        let response = await sendRequest("GET", `${HOST}/api/workouts/`);
        let data = await response.json();
        let workouts = data.results;    
        let athleteWorkouts = workouts.filter(w => w.owner === athleteUrl)
        let planned = athleteWorkouts.filter(w => w.planned === true).length;
        let done = athleteWorkouts.filter(w => w.done === true).length;
        let ca = athleteWorkouts.filter(w => w.workout_type === 'CA').length;
        let st = athleteWorkouts.filter(w => w.workout_type === 'ST').length;
        populatePlannedChart(done, planned - done);
        populateTypeChart(ca, st, athleteWorkouts.length - (ca+st));
        document.getElementById("planchart").style.display = "block";
        document.getElementById("typechart").style.display = "block";
    }else{
        document.getElementById("planchart").style.display = "none";
        document.getElementById("typechart").style.display = "none";  
    }
    
}
function populatePlannedChart(done, notDone){    
    var context = document.getElementById('plannedChart')
    new Chart(context, {
        type: "pie",
        data: {
            labels: ['Done', 'Not Done'],
            datasets: [{
                label: '# of Workouts',
                data: [done, notDone],
                backgroundColor: [
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 99, 132, 0.2)'
                    
                ],
                borderColor: [
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 99, 132, 1)'
                    
                ],
                borderWidth: 1
            }]
        },
        options: {
            rotation: 1
        }
    })
}

function populateTypeChart(cardio, strength, none){ 
    var context = document.getElementById('typeChart')
    new Chart(context, {
      type: "pie",
      data: {
        labels: ['Cardio', 'Strength', 'None'],
        datasets: [{
          label: '# of Workouts',
          data: [cardio, strength, none],
          backgroundColor: [
              'rgba(255, 99, 132, 0.2)',
              'rgba(54, 162, 235, 0.2)',
              'rgba(255, 206, 86, 0.2)'
          ],
          borderColor: [
              'rgba(255, 99, 132, 1)',
              'rgba(54, 162, 235, 1)',
              'rgba(255, 206, 86, 1)'
          ],
          borderWidth: 1
      }]
  },
  options: {
      rotation: 1,
  }
})
}

window.addEventListener("DOMContentLoaded", async () => {
    await getAthletes();
});