async function getAthletes() {
    let currentUser = await getCurrentUser();
    let athleteSelect = document.querySelector("#inputOwner");
    for(let athleteUrl of currentUser.athletes){
        let response = await sendRequest("GET", athleteUrl);
        let athlete = await response.json();
        let option = document.createElement("option");
        option.value = athlete.id;
        option.innerText = athlete.username;
        athleteSelect.append(option);
    }
}

function generateWorkoutForm() {    
    let form = document.querySelector("#form-workout");

    let formData = new FormData(form);
    let submitForm = new FormData();

    submitForm.append("name", formData.get('name'));
    let date = new Date(formData.get('date')).toISOString();
    submitForm.append("date", date);
    submitForm.append("notes", formData.get("notes"));
    submitForm.append("visibility", "CO");
    submitForm.append("workout_type", formData.get("workout_type"));
    submitForm.append("planned", formData.get("planned"));
    submitForm.set("planned", "True");
    submitForm.append("owner", formData.get('owner_username'))     


    // adding exercise instances
    let exerciseInstances = [];
    let exerciseInstancesTypes = formData.getAll("type");
    let exerciseInstancesSets = formData.getAll("sets");
    let exerciseInstancesNumbers = formData.getAll("number");
    for (let i = 0; i < exerciseInstancesTypes.length; i++) {
        exerciseInstances.push({
            exercise: `${HOST}/api/exercises/${exerciseInstancesTypes[i]}/`,
            number: exerciseInstancesNumbers[i],
            sets: exerciseInstancesSets[i]
        });
    }

    submitForm.append("exercise_instances", JSON.stringify(exerciseInstances));
    // adding files
    for (let file of formData.getAll("files")) {
        submitForm.append("files", file);
    }
    return submitForm;
}

async function createBlankExercise() {
    let selected = document.querySelector("#inputType2");

    let exerciseTemplate = document.querySelector("#template-exercise");
    let divExerciseContainer = exerciseTemplate.content.firstElementChild.cloneNode(true);
    let exerciseTypeSelect = divExerciseContainer.querySelector("select");

    let exerciseTypeResponse = await sendRequest("GET", `${HOST}/api/exercises/`);
    let exerciseTypes = await exerciseTypeResponse.json();

    selected.addEventListener('change', updateValue);

    function changeExercisetype(select){ //Changes available exercises based on "Type of exercise input"
    let filtered = exerciseTypes.results.filter(exercise => exercise.exercise_type === select);
    exerciseTypeSelect.length = 0;

        if (selected.value == "NN"){
            for (let i = 0; i < exerciseTypes.count; i++) {
                let option = document.createElement("option");
                option.value = exerciseTypes.results[i].id;
                option.innerText = exerciseTypes.results[i].name;
                exerciseTypeSelect.append(option);
            }
            let currentExerciseType = exerciseTypes.results[0];
            exerciseTypeSelect.value = currentExerciseType.name

        }
        else{
            for (let i = 0; i < filtered.length; i++) {
                let option = document.createElement("option");
                option.value = filtered[i].id;
                option.innerText = filtered[i].name;
                exerciseTypeSelect.append(option);
            }   
            let currentExerciseType = filtered[0];
            exerciseTypeSelect.value = currentExerciseType.name;
        }
    }

    function updateValue(e) {
        changeExercisetype(e.target.value);
    }

    let divExercises = document.querySelector("#div-exercises");
    divExercises.appendChild(divExerciseContainer);
}

function removeExercise(event) {
    let divExerciseContainers = document.querySelectorAll(".div-exercise-container");
    if (divExerciseContainers && divExerciseContainers.length > 0) {
        divExerciseContainers[divExerciseContainers.length - 1].remove();
    }
}

async function planWorkout() {
    let submitForm = generateWorkoutForm();

    let response = await sendRequest("POST", `${HOST}/api/workouts/planned/`, submitForm, "");

    if (response.ok) {
        window.location.replace("myathletes.html");
    } else {
        let data = await response.json();
        let alert = createAlert("Could not create new workout!", data);
        document.body.prepend(alert);
    }
}
function handleCancelDuringWorkoutCreate() {
    window.location.replace("myathletes.html");
}

window.addEventListener("DOMContentLoaded", async () => {
    await getAthletes();
    await createBlankExercise();
    setReadOnly(false, "#form-workout");
    let buttonAddExercise = document.querySelector("#btn-add-exercise");
    let buttonRemoveExercise = document.querySelector("#btn-remove-exercise");
    buttonAddExercise.addEventListener("click", createBlankExercise);
    buttonRemoveExercise.addEventListener("click", removeExercise);
    let cancelWorkoutButton = document.querySelector("#btn-cancel-workout");
    let okWorkoutButton = document.querySelector("#btn-ok-workout");
    
    okWorkoutButton.className = okWorkoutButton.className.replace(" hide", "");
    cancelWorkoutButton.className = cancelWorkoutButton.className.replace(" hide", "");
    buttonAddExercise.className = buttonAddExercise.className.replace(" hide", "");
    buttonRemoveExercise.className = buttonRemoveExercise.className.replace(" hide", "");
    okWorkoutButton.addEventListener("click", async () => await planWorkout());
    cancelWorkoutButton.addEventListener("click", handleCancelDuringWorkoutCreate);
});