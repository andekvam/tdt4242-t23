async function resetPassword(event){
    let form = document.querySelector("#form-reset-password");
    let formData = new FormData(form);
    let response = await sendRequest("POST", `${HOST}/api/password/reset/confirm/`, formData, "");
    if(!response.ok){
        let data = await response.json();
        let alert = createAlert("Failed to reset password!", data);
        document.body.prepend(alert); 
    }
    else{  
        window.location.replace("passwordchanged.html")
    }
}
document.querySelector("#btn-password-reset").addEventListener("click", async (event) => await resetPassword(event));