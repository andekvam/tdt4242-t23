// ***********************************************************
// This example support/index.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
import './commands'

var coachUser = {
    username: null,
    password: null,
    email: 'test@gmail.com',
    phone_number: '45789867',
    street_address: 'Trondheim street 1',
    city: 'Trondheim',
    country: 'Norway',
  }
  var athleteUser = {
    username: null,
    password: null,
    email: 'athlete@gmail.com',
    phone_number: '99578985',
    street_address: 'Bergen street 2',
    city: 'Bergen',
    country: 'Sweeden',
  }
  var baseUrl = "localhost:3000"
  
  // generateId :: Integer -> String
  function getRandomString(len=40) {
    const dec2hex = dec => dec.toString(16).padStart(2, "0")
  
    var arr = new Uint8Array(len / 2)
    window.crypto.getRandomValues(arr)
    return Array.from(arr, dec2hex).join('')
  }
  
  function generateUser() {
    return {
      username: `test_user_${getRandomString(10)}`,
      password: getRandomString(10),
    }
  }
  
  function registerUser(user) {
    cy.visit(`${baseUrl}/index.html`)
    cy.get('#btn-register').click()
  
    cy.url().should('include', 'register.html')
  
    cy.get('input[name="username"]').type(user.username)
    
    cy.get('input[name="password"]').type(user.password)
    cy.get('input[name="password1"]').type(user.password)
    
    cy.get('#btn-create-account').click()
    cy.url().should('include', 'workouts.html')

    logout()
  
  }
  
  function login(user) {
    cy.visit(`${baseUrl}/index.html`)
    cy.get('#btn-login-nav').click()
  
    cy.url().should('include', 'login.html')
  
    cy.get('input[name="username"]').type(user.username)
    
    cy.get('input[name="password"]').type(user.password)
    
    cy.intercept({method: 'GET', url: '/workouts.html' }).as('loginRequest')
    cy.get('#btn-login').click()
    cy.wait('@loginRequest')
  }
  
  function logout() {
    cy.get('#btn-logout', { timeout: 10000 }).click()
    cy.url().should('include', 'index.html')
  }
  
  
  before(() => {
    coachUser = {
      ...coachUser, 
      ...generateUser()
    }
    athleteUser = {
      ...athleteUser, 
      ...generateUser()
    }
    registerUser(coachUser)
    registerUser(athleteUser)
    cy.visit(`${baseUrl}/index.html`)
  })
  
  beforeEach(() => {
    cy.wrap(coachUser).as('coachUser')
    cy.wrap(athleteUser).as('athleteUser')
    cy.wrap(login).as('login')
  })
  
  