var planAthleteWorkout = {
  name: "Test",
  notes: "Testing"
}

describe('Choosing type of workout/exercise', () => {
    it('Should allow choosing exercise type when making exercise', function() {
      this.login(this.athleteUser)
      cy.get('#nav-exercises').click()
      cy.url().should('include', 'exercises.html')
      cy.get('#btn-create-exercise').click()
      cy.url().should('include', 'exercise.html')
      cy.get('input[name="name"]').type("Deadlifts")
      cy.get('textarea[name="description"]').type("Lift")
      cy.get('input[name="unit"]').type("Kg")
      cy.get('select[name="exercise_type"]').select("Strength")

      cy.intercept({method: 'POST', url: '/api/exercises/' }).as('createRequest')
      cy.get('#btn-ok-exercise').click()
      cy.wait('@createRequest')
      cy.url().should('include', 'exercises.html')
    }),

    it('Should allow choosing a specific training type when logging a workout', function() {
      this.login(this.athleteUser)
      cy.get('#nav-workouts').click()
      cy.url().should('include', 'workouts.html')
      cy.get('#btn-create-workout').click()
      cy.url().should('include', 'workout.html')
      cy.get('input[name="name"]').type(planAthleteWorkout.name)
      cy.get('input[name="date"]').type("2021-06-14T00:00")
      cy.get('textarea[name="notes"]').type(planAthleteWorkout.notes)
      cy.get('select[name="workout_type"]').select("Strength")
      cy.get('select[name="type"]')
        .children()
        .eq(1)
      cy.get('input[name="sets"]').type(3)
      cy.get('input[name="number"]').type(10)
      cy.get('#btn-add-exercise').click()

       cy.intercept({method: 'POST', url: '/api/workouts/' }).as('createRequest')
      cy.get('#btn-ok-workout').click()
      cy.wait('@createRequest')
      cy.url().should('include', 'workouts.html')
    })
})