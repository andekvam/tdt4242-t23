describe('Password reset function', () => {
    it('Should be able to enter email to recive "reset password"-information ', function() {
      cy.url().should('include', 'index.html')
      cy.get('#btn-login-nav').click()
      cy.url().should('include', 'login.html')
      cy.contains('Forgot your password?').click()
      cy.get('input[name="email"]').type('tester@gmail.com')
      cy.get('#btn-reset-password').click()
      cy.url().should('include', 'resetconfirmed.html')
    })
})