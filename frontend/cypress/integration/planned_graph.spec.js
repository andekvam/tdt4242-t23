
/*System test for planning a workout for athlete and statistics page */
var planAthleteWorkout = {
    name: "Test",
    notes: "Testing"
}

describe('Athletes/coach page', () => {
    it('Should allow athlete-adding', function() {
      this.login(this.coachUser)
      cy.get('#nav-myathletes').click()
      cy.url().should('include', 'myathletes.html')
      cy.get('input[name="athlete"]:not([disabled])', { timeout: 10000 })
        .focus()
        .type(this.athleteUser.username)
        .parent()
        .children('button')
        .first()
        .click()
      
      cy.get('#button-submit-roster')
        .click()
    })
  
    it('Should allow coach accepting', function() {
      this.login(this.athleteUser)
      cy.get('#nav-mycoach').click()
      cy.url().should('include', 'mycoach.html')
      cy.intercept({method: 'PATCH'}).as('acceptOfferRequest')
      cy.get('#list-offers', { timeout: 10000 })
        .children()
        .first()
        .should('contain.text', this.coachUser.username)
        .should('contain.text', 'wants to be your coach')
        .children('.btn-group')
        .first()
        .children('.btn.btn-success')
        .first()
        .should('contain.text', 'Accept')
        .click()
      
      cy.wait('@acceptOfferRequest')
      cy.wait(500)
  
      cy.get('#input-coach').should('contain.value', this.coachUser.username)
    })

    it('Shoud allow coach to plan a workout for athlete', function() {
        this.login(this.coachUser)
        cy.get('#nav-myathletes').click()
        cy.url().should('include', 'myathletes.html')
        cy.get('#btn-plan-workout', { timeout: 10000 }).click()
        cy.visit(`localhost:3000/plannedworkout.html`)
        cy.get('input[name="name"]').type(planAthleteWorkout.name)
        cy.get('input[name="date"]').type("2021-06-14T00:00")
        cy.get('textarea[name="notes"]').type(planAthleteWorkout.notes)
        cy.get('#btn-ok-workout').click()
        cy.url().should('include', 'myathletes.html')
        cy.get('#nav-workouts').click()
    })

    it('Should allow athlete to mark planned workout as done', function() {
        this.login(this.athleteUser)
        cy.get('#nav-workouts').click()
        cy.get('#btn-confirm-done').click()
    })

    it('Should allow coach to se statistiv on their athlete', function(){
        this.login(this.coachUser)
        cy.get('#nav-stats').click()
        cy.get('#athleteselect').select(this.athleteUser.username)
        cy.get('#plannedChart').should('be.visible')
        cy.get('#typeChart').should('be.visible')
    })
        

})
  