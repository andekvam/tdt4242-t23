"""
Tests for the workouts application.
"""
import json
from django.test import TestCase, Client
from workouts.models import Workout, WorkoutFile
from users.models import User
from comments.models import Comment
from secfit import constants

class WorkoutCreationBoundary(TestCase):    
    def setUp(self):
        self.client = Client()
        self.owner = User.objects.create(username="user")
        self.owner.set_password("password")
        self.owner.save()
        response = self.client.post("/api/token/", {"username": "user", "password": "password"})
        self.client.defaults["HTTP_AUTHORIZATION"] = "Bearer " + json.loads(response.content)["access"]

        self.data = {
            "name": "Test Workout",
            "date": "2021-03-08T18:56",
            "notes": "Test",
            "owner": self.owner,
            "visibility": "PU",
            "exercise_instances": "[]",
            "workout_type": "ST"
        }

    def test_normal_workout(self):
        response = self.client.post(constants.API_WORKOUTS, self.data)
        self.assertEqual(response.status_code, 201)

    def test_workout_name(self):
        # Empty name, should fail
        self.data["name"] = ""
        response = self.client.post(constants.API_WORKOUTS, self.data)
        self.assertEqual(response.status_code, 400)

        # Min length name, should pass
        self.data["name"] = "W"
        response = self.client.post(constants.API_WORKOUTS, self.data)
        self.assertEqual(response.status_code, 201)

        # Max length name (100 chars), should pass
        self.data["name"] = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
        response = self.client.post(constants.API_WORKOUTS, self.data)
        self.assertEqual(response.status_code, 201)

        # Max length + 1 name, should fail
        self.data["name"] = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
        response = self.client.post(constants.API_WORKOUTS, self.data)
        self.assertEqual(response.status_code, 400)

    def test_workout_date(self):
        # Empty date, should fail
        self.data["date"] = ""
        response = self.client.post(constants.API_WORKOUTS, self.data)
        self.assertEqual(response.status_code, 400)

        # Min date, should pass as there is no validation
        self.data["date"] = "0001-01-01T00:00"
        response = self.client.post(constants.API_WORKOUTS, self.data)
        self.assertEqual(response.status_code, 201)

        # Max date, should pass as there is no validation
        self.data["date"] = "9999-12-31T23:59"
        response = self.client.post(constants.API_WORKOUTS, self.data)
        self.assertEqual(response.status_code, 201)

        # Max date + 1 for invalid date, should fail 
        self.data["date"] = "10000-01-01T00:00"
        response = self.client.post(constants.API_WORKOUTS, self.data)
        self.assertEqual(response.status_code, 400)

    def test_workout_notes(self):
        # Empty notes, should fail
        self.data["notes"] = ""
        response = self.client.post(constants.API_WORKOUTS, self.data)
        self.assertEqual(response.status_code, 400)

        # Min length note, should pass
        self.data["notes"] = "a"
        response = self.client.post(constants.API_WORKOUTS, self.data)
        self.assertEqual(response.status_code, 201)

    def test_workout_visibility(self):
        #Empty visibility, should fail
        self.data["visibility"] = ""
        response = self.client.post(constants.API_WORKOUTS, self.data)
        self.assertEqual(response.status_code, 400)

        # Min and max approved visibility, should pass
        self.data["visibility"] = "CO"
        response = self.client.post(constants.API_WORKOUTS, self.data)
        self.assertEqual(response.status_code, 201)

        # Min - 1 visibility, should fail
        self.data["visibility"] = "C"
        response = self.client.post(constants.API_WORKOUTS, self.data)
        self.assertEqual(response.status_code, 400)

        # Max + 1 visiblity, should fail 
        self.data["visibility"] = "COO"
        response = self.client.post(constants.API_WORKOUTS, self.data)
        self.assertEqual(response.status_code, 400)
    
    def test_workout_type(self):
        # Empty workout type, should fail
        self.data["workout_type"] = ""
        response = self.client.post(constants.API_WORKOUTS, self.data)
        self.assertEqual(response.status_code, 400)

        # Min and max approved workout type, should pass
        self.data["workout_type"] = "CA"
        response = self.client.post(constants.API_WORKOUTS, self.data)
        self.assertEqual(response.status_code, 201)

        # Min - 1 workout type, should fail
        self.data["workout_type"] = "C"
        response = self.client.post(constants.API_WORKOUTS, self.data)
        self.assertEqual(response.status_code, 400)

        # Max + 1 workout type, should fail
        self.data["workout_type"] = "CAA"
        response = self.client.post(constants.API_WORKOUTS, self.data)
        self.assertEqual(response.status_code, 400)
