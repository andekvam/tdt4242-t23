from django.test import TestCase, Client
from django.urls import reverse
from rest_framework.test import APIRequestFactory, APITestCase
from workouts.models import Workout, WorkoutFile, Exercise, ExerciseInstance
from django.contrib.auth import get_user_model
from datetime import datetime, timezone
from django.core.exceptions import ValidationError
import json
from secfit import constants

"""
Integration
"""

class TestPlannedWorkoutImplementation(TestCase):
    def setUp(self):
        self.factory = APIRequestFactory()
        self.client = Client()
        self.user = get_user_model().objects.create(username="test")
        self.user2 = get_user_model().objects.create(username="test2")
        self.user.set_password("testpassword")
        self.user.save()

        self.workout = Workout.objects.create(name="workout1", owner=self.user, date=datetime.now(timezone.utc), planned="True", done="True")
        self.workout2 = Workout.objects.create(name="workout2", owner=self.user, date=datetime.now(timezone.utc), planned="False", done="False")

        response = self.client.post("/api/token/", {"username": "test", "password": "testpassword"})
        self.client.defaults["HTTP_AUTHORIZATION"] = "Bearer " + json.loads(response.content)["access"]
    
    def test_is_planned(self):
        response = self.client.get(constants.API_WORKOUTS + str(self.workout.id) + "/")
        with self.subTest():
            self.assertEqual(response.status_code, 200)
        with self.subTest():
            content = json.loads(response.content)
            self.assertTrue(content['planned'])
        
    def test_is_done(self):
        response = self.client.get(constants.API_WORKOUTS + str(self.workout.id) + "/")
        with self.subTest():
            self.assertEqual(response.status_code, 200)
        with self.subTest():
            content = json.loads(response.content)
            self.assertTrue(content['done'])
    
    def test_is_not_planned(self):
        response = self.client.get(constants.API_WORKOUTS + str(self.workout2.id) + "/")
        with self.subTest():
            self.assertEqual(response.status_code, 200)
        with self.subTest():
            content = json.loads(response.content)
            self.assertFalse(content['planned'])

    def test_is_not_done(self):
        response = self.client.get(constants.API_WORKOUTS + str(self.workout2.id) + "/")
        with self.subTest():
            self.assertEqual(response.status_code, 200)
        with self.subTest():
            content = json.loads(response.content)
            self.assertFalse(content['done'])

class TestExerciseType(TestCase):
    def setUp(self):
        self.client = Client()
        self.client = Client()
        self.user = get_user_model().objects.create(username="test")
        self.user2 = get_user_model().objects.create(username="test2")
        self.user.set_password("testpassword")
        self.user.save()

        self.exercise = Exercise.objects.create(name="Running", description="test", unit="reps", exercise_type="CA")
        self.exercise2 = Exercise.objects.create(name="Squats", exercise_type="ST")
        self.exercise3 = Exercise.objects.create(name="Bench press", exercise_type="NN")
        self.exercise4 = Exercise.objects.create(name="not valid", description="test", unit="reps", exercise_type="XX")

        self.workout = Workout.objects.create(name="workout1", owner=self.user, date=datetime.now(timezone.utc), planned="True", done="True", workout_type="CA")
        self.workout2 = Workout.objects.create(name="workout2", owner=self.user, date=datetime.now(timezone.utc), planned="True", done="False")

        response = self.client.post("/api/token/", {"username": "test", "password": "testpassword"})
        self.client.defaults["HTTP_AUTHORIZATION"] = "Bearer " + json.loads(response.content)["access"]

    def test_valid_exercise(self):
        try:
            self.exercise.full_clean()
        except ValidationError as e:
            self.assertTrue('name' in e.message_dict)
         
    def test_invalid_exercise(self):
        try:
            self.exercise4.full_clean()
        except ValidationError as e:
            self.assertFalse('name' in e.message_dict)
    
    def test_valid_exercise_workou(self):
        response = self.client.get(constants.API_WORKOUTS + str(self.workout.id) + "/")
        with self.subTest():
            self.assertEqual(response.status_code, 200)
        with self.subTest():
            content = json.loads(response.content)
            self.assertEqual(content['workout_type'], "CA")
    



    




    

        