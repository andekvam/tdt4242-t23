import json
from django.test import TestCase, Client
from django.forms.models import model_to_dict
from workouts.models import Workout, WorkoutFile, Exercise
from users.models import User
import datetime
from secfit import constants


class WorkoutIntegrationTest(TestCase):
    def setUp(self):
        self.authClient = Client()
        self.unauthClient = Client()
        
        self.user = User.objects.create(username="user")
        self.user.set_password("test")
        self.user.save()
        response = self.authClient.post("/api/token/", {"username": "user", "password": "test"})
        self.authClient.defaults["HTTP_AUTHORIZATION"] = "Bearer " + json.loads(response.content)["access"]
        self.user2 = User.objects.create(username="user2", password="test2")

        self.validWorkoutBody = {"name": "Workout test","date": constants.DATE,"notes": "Test","visibility": "PU","workout_type": "ST","exercise_instances": []}
        self.invalidWorkoutBody = {"date": constants.DATE,"notes": "Test","visibility": "PU","workout_type": "ST","exercise_instances": []}
        self.editedWorkoutBody = {"name": "Coach workout","date": constants.DATE,"notes": "Test","visibility": "CO","workout_type": "ST","exercise_instances": []}

        self.exercise = Exercise.objects.create(name="Exercise", description="Description", unit="Unit", exercise_type="ST")

        self.exercise_instance = { "exercise": f"http://testserver/api/exercises/{self.exercise.id}/", "number": 2, "sets": 3 }
        self.editedWorkoutBodyWithTwoExercises = {"name": "Coach workout","date": constants.DATE,"notes": "Test","visibility": "CO","workout_type": "ST","exercise_instances": [ self.exercise_instance, self.exercise_instance ]}
        self.editedWorkoutBodyWithOneExercise = {"name": "Coach workout","date": constants.DATE,"notes": "Test","visibility": "CO","workout_type": "ST","exercise_instances": [ self.exercise_instance ]}
        
        
        self.workout = Workout.objects.create(name="Public workout", owner=self.user, visibility="PU", date=constants.DATE, notes="Test", workout_type="ST")
        self.workout2 = Workout.objects.create(name="Public workout", owner=self.user2, visibility="PU", date=constants.DATE)

    def test_workout_creation(self):
        with self.subTest("Authenticated user can create workout"):
            response = self.authClient.post(constants.API_WORKOUTS, self.validWorkoutBody, content_type=constants.CONTENT_TYPE)
            self.assertEqual(response.status_code, 201)
        with self.subTest("Authenticated user can not create workout if required fields are missing"):
            response = self.authClient.post(constants.API_WORKOUTS, self.invalidWorkoutBody, content_type=constants.CONTENT_TYPE)
            self.assertEqual(response.status_code, 400)
        with self.subTest("Unauthenticated user can not create workout"):
            response = self.unauthClient.post(constants.API_WORKOUTS, self.validWorkoutBody, content_type=constants.CONTENT_TYPE)
            self.assertEqual(response.status_code, 401)

    def test_fetch_workout(self):
        with self.subTest("Authenticated user can fetch workouts"):
            response = self.authClient.get(constants.API_WORKOUTS)
            self.assertEqual(response.status_code, 200)

        with self.subTest("Authenticated user can fetch workout"):
            response = self.authClient.get(constants.API_WORKOUTS + str(self.workout.id) + "/")
            self.assertEqual(response.status_code, 200)

        with self.subTest("Fetched workout corresponds to database entry"):
            response = self.authClient.get(constants.API_WORKOUTS + str(self.workout.id) + "/")
            workout = model_to_dict(Workout.objects.get(pk=1))
            # Format date to match expected value
            workout["date"] = workout["date"].isoformat().replace('+00:00', 'Z')
            # Need to remove fields that are not present in database entry and change url to owner id
            response_dict = json.loads(response.content.decode())
            del response_dict["url"]
            del response_dict["owner_username"]
            del response_dict["exercise_instances"]
            del response_dict["files"]
            response_dict["owner"] = int(response_dict["owner"].split("/")[-2])
            self.assertEqual(response_dict, workout)
        
        with self.subTest("Unauthenticated user can not fetch workouts"):
            response = self.unauthClient.get(constants.API_WORKOUTS)
            self.assertEqual(response.status_code, 401)

        with self.subTest("Unauthenticated user can not fetch workout"):
            response = self.unauthClient.get(constants.API_WORKOUTS + str(self.workout.id) + "/")
            self.assertEqual(response.status_code, 401)

    def test_edit_workout(self):
        with self.subTest("Authenticated user can edit workout that they own"):
            response = self.authClient.put(constants.API_WORKOUTS + str(self.workout.id) + "/", self.editedWorkoutBody, content_type=constants.CONTENT_TYPE)
            self.assertEqual(response.status_code, 200)
        
        with self.subTest("Edited workout is stored correctly in database"):
            response = self.authClient.get(constants.API_WORKOUTS + str(self.workout.id) + "/")
            # Only checks altered fields
            self.assertEqual(json.loads(response.content.decode())["name"], self.editedWorkoutBody["name"])
            self.assertEqual(json.loads(response.content.decode())["visibility"], self.editedWorkoutBody["visibility"])

        with self.subTest("Exercise instances are included in workout after edit"):
            response = self.authClient.put(constants.API_WORKOUTS + str(self.workout.id) + "/", self.editedWorkoutBodyWithTwoExercises, content_type=constants.CONTENT_TYPE)
            response_dict = json.loads(response.content.decode())
            self.assertEqual(len(response_dict["exercise_instances"]), 2)
            
        with self.subTest("Exercise instances are removed in workout after edit"):
            response = self.authClient.put(constants.API_WORKOUTS + str(self.workout.id) + "/", self.editedWorkoutBodyWithOneExercise, content_type=constants.CONTENT_TYPE)
            response_dict = json.loads(response.content.decode())
            self.assertEqual(len(response_dict["exercise_instances"]), 1)

        with self.subTest("Authenticated user can not edit workout that they do not own"):
            response = self.authClient.put(constants.API_WORKOUTS + str(self.workout2.id) + "/", self.editedWorkoutBody, content_type=constants.CONTENT_TYPE)
            self.assertEqual(response.status_code, 403)

        with self.subTest("Unauthenticated user can not edit workout"):
            response = self.unauthClient.put(constants.API_WORKOUTS + str(self.workout2.id) + "/", self.editedWorkoutBody, content_type=constants.CONTENT_TYPE)
            self.assertEqual(response.status_code, 401)

    def test_delete_workout(self):
        with self.subTest("Authenticated user can delete workout that they own"):
            response = self.authClient.delete(constants.API_WORKOUTS + str(self.workout.id) + "/")
            self.assertEqual(response.status_code, 204)

        with self.subTest("Workout can not be fetched after deletion"):
            response = self.authClient.get(constants.API_WORKOUTS + str(self.workout.id) + "/")
            self.assertEqual(response.status_code, 404)

        with self.subTest("Authenticated user can not delete workout that they do not own"):
            response = self.authClient.delete(constants.API_WORKOUTS + str(self.workout2.id) + "/")
            self.assertEqual(response.status_code, 403)

        with self.subTest("Unauthenticated user can not delete workout"):
            response = self.unauthClient.delete(constants.API_WORKOUTS + str(self.workout2.id) + "/")
            self.assertEqual(response.status_code, 401)

class ExerciseIntegrationTest(TestCase):
    def setUp(self):
        self.authClient = Client()
        self.unauthClient = Client()
        
        self.user = User.objects.create(username="user")
        self.user.set_password("test")
        self.user.save()
        response = self.authClient.post("/api/token/", {"username": "user", "password": "test"})
        self.authClient.defaults["HTTP_AUTHORIZATION"] = "Bearer " + json.loads(response.content)["access"]

        self.validExerciseBody = {"name": "Test exercise","description": "test","unit": "test","exercise_type": "CA"}
        self.invalidExerciseBody = {"description": "test","unit": "test","exercise_type": "CA"}
        self.editedExerciseBody = {"name": "Exercise","description": "test","unit": "New Unit","exercise_type": "ST"}
        
        self.exercise = Exercise.objects.create(name="Exercise", description="Description", unit="Unit", exercise_type="ST")

    def test_exercise_creation(self):
        with self.subTest("Authenticated user can create exercise"):
            response = self.authClient.post(constants.API_EXERCISES, self.validExerciseBody, content_type=constants.CONTENT_TYPE)
            self.assertEqual(response.status_code, 201)

        with self.subTest("Authenticated user can not create exercise if required fields are missing"):
            response = self.authClient.post(constants.API_EXERCISES, self.invalidExerciseBody, content_type=constants.CONTENT_TYPE)
            self.assertEqual(response.status_code, 400)

        with self.subTest("Unauthenticated user can not create exercise"):
            response = self.unauthClient.post(constants.API_EXERCISES, self.validExerciseBody, content_type=constants.CONTENT_TYPE)
            self.assertEqual(response.status_code, 401)

    def test_fetch_exercise(self):
        with self.subTest("Authenticated user can fetch exercises"):
            response = self.authClient.get(constants.API_EXERCISES)
            self.assertEqual(response.status_code, 200)

        with self.subTest("Authenticated user can fetch specific exercise"):
            response = self.authClient.get(constants.API_EXERCISES + str(self.exercise.id) + "/")
            self.assertEqual(response.status_code, 200)
        
        with self.subTest("Fetched exercise corresponds to database entry"):
            response = self.authClient.get(constants.API_EXERCISES + str(self.exercise.id) + "/")
            exercise = model_to_dict(Exercise.objects.get(pk=1))
            response_dict = json.loads(response.content.decode())
            del response_dict["url"]
            del response_dict["instances"]
            self.assertEqual(response_dict, exercise)

        with self.subTest("Unauthenticated user can not fetch exercises"):
            response = self.unauthClient.get(constants.API_EXERCISES)
            self.assertEqual(response.status_code, 401)
        
        with self.subTest("Unauthenticated user can not fetch specific exercise"):
            response = self.unauthClient.get(constants.API_EXERCISES + str(self.exercise.id) + "/")
            self.assertEqual(response.status_code, 401)

    def test_edit_exercise(self):
        with self.subTest("Authenticated user can edit exercise"):
            response = self.authClient.put(constants.API_EXERCISES + str(self.exercise.id) + "/", self.editedExerciseBody, content_type=constants.CONTENT_TYPE)
            self.assertEqual(response.status_code, 200)

        with self.subTest("Edited exercise fields are stored"):
            response = self.authClient.get(constants.API_EXERCISES+ str(self.exercise.id) + "/")
            self.assertEqual(json.loads(response.content.decode())["unit"], self.editedExerciseBody["unit"])

        with self.subTest("Unauthenticated user can not edit exercise"):
            response = self.unauthClient.put(constants.API_EXERCISES + str(self.exercise.id) + "/", self.editedExerciseBody, content_type=constants.CONTENT_TYPE)
            self.assertEqual(response.status_code, 401)

    def test_delete_exercise(self):
        with self.subTest("Unauthenticated user can not delete exercise"):
            response = self.unauthClient.delete(constants.API_EXERCISES + str(self.exercise.id) + "/")
            self.assertEqual(response.status_code, 401)

        with self.subTest("Authenticated user can delete exercise"):
            response = self.authClient.delete(constants.API_EXERCISES + str(self.exercise.id) + "/")
            self.assertEqual(response.status_code, 204)

        with self.subTest("Exercise can not be fetched after deletion"):
            response = self.authClient.get(constants.API_EXERCISES + str(self.exercise.id) + "/")
            self.assertEqual(response.status_code, 404)

