"""
Tests for the workouts application.
"""
from django.urls import reverse
from rest_framework.test import APIRequestFactory, APITestCase
from workouts.models import Workout, WorkoutFile
from django.contrib.auth import get_user_model
from workouts.permissions import IsOwner, IsOwnerOfWorkout, IsCoachAndVisibleToCoach, IsCoachOfWorkoutAndVisibleToCoach, IsPublic, IsWorkoutPublic, IsReadOnly
from datetime import datetime, timezone
from secfit import constants

class IsOwnerTestCase(APITestCase):

    def setUp(self):
        self.permission = IsOwner()
        self.factory = APIRequestFactory()
        self.user = get_user_model().objects.create(username="test")
        self.user2 = get_user_model().objects.create(username="test2")
        self.workout = Workout.objects.create(name="test", owner=self.user, date=datetime.now(timezone.utc))


    def test_has_object_permission(self):
        request = self.factory.get('api/workouts/')
        request.user = self.user
        self.assertTrue(self.permission.has_object_permission(request, request, self.workout))
        request2 = self.factory.get('api/workouts/')
        request2.user = self.user2
        self.assertFalse(self.permission.has_object_permission(request2, request2, self.workout))

class IsOwnerOfWorkoutTestCase(APITestCase):

    def setUp(self):
        self.permission = IsOwnerOfWorkout()
        self.factory = APIRequestFactory()
        self.user = get_user_model().objects.create(username="test")
        self.user2 = get_user_model().objects.create(username="test2")
        self.workout = Workout.objects.create(name="test", owner=self.user, date=datetime.now(timezone.utc))
        self.workoutFile = WorkoutFile.objects.create(workout=self.workout, owner=self.user, file=None)

    def test_has_permission(self):
        url = reverse('workout-file-list')
        #Check True if owner match request user
        data = {
            "workout" : "api/workouts/1/",
            "owner" : "api/users/1/",
        }
        request = self.factory.post(url)
        request.user = self.user
        request.data = data
        self.assertTrue(self.permission.has_permission(request, None))
        #Check False if request user != owner
        request.user = self.user2
        self.assertFalse(self.permission.has_permission(request, None))
        #Check False when workout is missing from data
        request2 = self.factory.post(url)
        request2.user = self.user2
        data2 = {
            "owner" : "api/users/2/"
        }
        request2.data = data2
        self.assertFalse(self.permission.has_permission(request2, None))
        #Check other than POST
        request3 = self.factory.get(url)
        self.assertTrue(self.permission.has_permission(request3, None))
    
    def test_has_object_permission(self):
        request = self.factory.get(constants.API_WORKOUT_FILES)
        request.user = self.user
        self.assertTrue(self.permission.has_object_permission(request, request, self.workoutFile))
        request2 = self.factory.get(constants.API_WORKOUT_FILES)
        request2.user = self.user2
        self.assertFalse(self.permission.has_object_permission(request2, request2, self.workoutFile))

class IsCoachAndVisibleToCoachTestCase(APITestCase):

    def setUp(self):
        self.permission = IsCoachAndVisibleToCoach()
        self.factory = APIRequestFactory()
        self.user = get_user_model().objects.create(username="test")
        self.user2 = get_user_model().objects.create(username="test2", coach=self.user)
        self.workout = Workout.objects.create(name="test", owner=self.user2, date=datetime.now(timezone.utc), visibility='CO')

    def test_has_object_permission(self):
        request = self.factory.get('api/workouts/1/')
        request.user = self.user
        self.assertTrue(self.permission.has_object_permission(request, None, self.workout))
        request2 = self.factory.get('api/workouts/1/')
        request2.user = self.user2
        self.assertFalse(self.permission.has_object_permission(request2, None, self.workout))

class IsCoachOfWorkoutAndVisibleToCoachTestCase(APITestCase):

    def setUp(self):
        self.permission = IsCoachOfWorkoutAndVisibleToCoach()
        self.factory = APIRequestFactory()
        self.user = get_user_model().objects.create(username="test")
        self.user2 = get_user_model().objects.create(username="test2", coach=self.user)
        self.workout = Workout.objects.create(name="test", owner=self.user2, date=datetime.now(timezone.utc))
        self.workoutFile = WorkoutFile.objects.create(workout=self.workout, owner=self.user, file=None)

    def test_has_object_permission(self):
        request = self.factory.get(constants.API_WORKOUT_FILES)
        request.user = self.user
        self.assertTrue(self.permission.has_object_permission(request, None, self.workoutFile))

class IsPublicTestCase(APITestCase):

    def setUp(self):
        self.permission = IsPublic()
        self.factory = APIRequestFactory()
        self.user = get_user_model().objects.create(username="test")
        self.workout = Workout.objects.create(name="test", owner=self.user, date=datetime.now(timezone.utc), visibility='CO')
        self.workout2 = Workout.objects.create(name="test2", owner=self.user, date=datetime.now(timezone.utc), visibility='PU')

    def test_has_object_permission(self):
        self.assertTrue(self.permission.has_object_permission(None, None, self.workout2))
        self.assertFalse(self.permission.has_object_permission(None, None, self.workout))

class IsWorkoutPublicTestCase(APITestCase):

    def setUp(self):
        self.permission = IsWorkoutPublic()
        self.factory = APIRequestFactory()
        self.user = get_user_model().objects.create(username="test")
        self.workout = Workout.objects.create(name="test", owner=self.user, date=datetime.now(timezone.utc), visibility='CO')
        self.workout2 = Workout.objects.create(name="test2", owner=self.user, date=datetime.now(timezone.utc), visibility='PU')
        self.workoutFile = WorkoutFile.objects.create(workout=self.workout, owner=self.user, file=None)
        self.workoutFile2 = WorkoutFile.objects.create(workout=self.workout2, owner=self.user, file=None)

    def test_has_object_permission(self):
        self.assertFalse(self.permission.has_object_permission(None, None, self.workoutFile))
        self.assertTrue(self.permission.has_object_permission(None, None, self.workoutFile2))

class IsReadOnlyTestCase(APITestCase):

    def setUp(self):
        self.permission = IsReadOnly()
        self.factory = APIRequestFactory()

    def test_has_object_permission(self):
        url = reverse('workout-list')
        request = self.factory.get(url)
        request2 = self.factory.post(url)
        self.assertTrue(self.permission.has_object_permission(request, None, None))
        self.assertFalse(self.permission.has_object_permission(request2, None, None))

# Create your tests here.
