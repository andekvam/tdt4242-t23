"""
Tests for the workouts application.
"""
import json
from django.test import TestCase, Client
from workouts.models import Workout, WorkoutFile
from users.models import User
from comments.models import Comment
from secfit import constants

class AthleteVisibilityTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.athlete = User.objects.create(username="athlete")
        self.athlete.set_password("athletepassword")
        self.athlete.save()
        self.otherathlete = User.objects.create(username="otherathlete", password="athletepassword")

        self.private_workout = Workout.objects.create(name="Private workout", owner=self.otherathlete, visibility="PR", date=constants.DATE)
        self.private_comment = Comment.objects.create(content="Test", workout=self.private_workout, timestamp=constants.DATE, owner=self.otherathlete)
        self.private_file = WorkoutFile.objects.create(owner=self.otherathlete, workout=self.private_workout, file=constants.FILE_PATH)

        self.private_own_workout = Workout.objects.create(name="Private own workout", owner=self.athlete, visibility="PR", date=constants.DATE)
        self.own_private_comment = Comment.objects.create(content="Test", workout=self.private_own_workout, timestamp=constants.DATE, owner=self.athlete)
        self.own_private_file = WorkoutFile.objects.create(owner=self.athlete, workout=self.private_own_workout, file=constants.FILE_PATH)

        self.public_workout = Workout.objects.create(name="Public workout", owner=self.otherathlete, visibility="PU", date=constants.DATE)
        self.public_comment = Comment.objects.create(content="Test", workout=self.public_workout, timestamp=constants.DATE, owner=self.athlete)
        self.public_file = WorkoutFile.objects.create(owner=self.athlete, workout=self.public_workout, file=constants.FILE_PATH)


        response = self.client.post("/api/token/", {"username": "athlete", "password": "athletepassword"})
        self.client.defaults["HTTP_AUTHORIZATION"] = "Bearer " + json.loads(response.content)["access"]
    def test_private_workout(self):
        with self.subTest("Fetching someones private workout"):
            response = self.client.get(constants.API_WORKOUTS + str(self.private_workout.id) +"/")
            self.assertEqual(response.status_code, 403)
        with self.subTest("Fetching someones private comment"):
            comment_response = self.client.get(constants.API_COMMENTS + str(self.private_comment.id) + "/")
            self.assertEqual(comment_response.status_code, 403)
        with self.subTest("Fetching someones private workout-file"):
            file_response = self.client.get(constants.API_WORKOUT_FILES + str(self.private_file.id) + "/")
            self.assertEqual(file_response.status_code, 403)
        
    def test_own_workout(self):
        with self.subTest("Fetching private workout"):
            response = self.client.get(constants.API_WORKOUTS + str(self.private_own_workout.id) +"/")
            self.assertEqual(response.status_code, 200)
        with self.subTest("Fetching private comment"):
            comment_response = self.client.get(constants.API_COMMENTS + str(self.own_private_comment.id) + "/")
            self.assertEqual(comment_response.status_code, 200)
        with self.subTest("Fetching private workout-file"):
            file_response = self.client.get(constants.API_WORKOUT_FILES + str(self.own_private_file.id) + "/")
            self.assertEqual(file_response.status_code, 200)
            
    def test_public_workout(self):
        with self.subTest("Fetching public workout"):
            response = self.client.get(constants.API_WORKOUTS + str(self.public_workout.id) +"/")
            self.assertEqual(response.status_code, 200)
        with self.subTest("Fetching public comment"):
            comment_response = self.client.get(constants.API_COMMENTS + str(self.public_comment.id) + "/")
            self.assertEqual(comment_response.status_code, 200)
        with self.subTest("Fetching public workout-file"):
            file_response = self.client.get(constants.API_WORKOUT_FILES + str(self.public_file.id) + "/")
            self.assertEqual(file_response.status_code, 200)       


class CoachVisibilityTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.coach = User.objects.create(username="coach")
        self.coach.set_password("coachpassword")
        self.coach.save()
        self.athlete = User.objects.create(username="athlete", password="athletepassword", coach=self.coach)

        self.private_workout = Workout.objects.create(name="Private workout", owner=self.athlete, visibility="PR", date=constants.DATE)
        self.private_comment = Comment.objects.create(content="Test", workout=self.private_workout, timestamp=constants.DATE, owner=self.athlete)
        self.private_file = WorkoutFile.objects.create(owner=self.athlete, workout=self.private_workout, file=constants.FILE_PATH)

        self.athlete_non_private_workout = Workout.objects.create(name="Private own workout", owner=self.athlete, visibility="CO", date=constants.DATE)
        self.non_private_comment = Comment.objects.create(content="Test", workout=self.athlete_non_private_workout, timestamp=constants.DATE, owner=self.athlete)
        self.non_private_file = WorkoutFile.objects.create(owner=self.athlete, workout=self.athlete_non_private_workout, file=constants.FILE_PATH)

        self.public_workout = Workout.objects.create(name="Public workout", owner=self.athlete, visibility="PU", date=constants.DATE)
        self.public_comment = Comment.objects.create(content="Test", workout=self.public_workout, timestamp=constants.DATE, owner=self.athlete)
        self.public_file = WorkoutFile.objects.create(owner=self.athlete, workout=self.public_workout, file=constants.FILE_PATH)

        response = self.client.post("/api/token/", {"username": "coach", "password": "coachpassword"})
        self.client.defaults["HTTP_AUTHORIZATION"] = "Bearer " + json.loads(response.content)["access"]

    def test_private_workout(self):
        with self.subTest("Fetching athletes private workout"):
            workout_response = self.client.get(constants.API_WORKOUTS + str(self.private_workout.id) +"/")
            self.assertEqual(workout_response.status_code, 403)
        with self.subTest("Fetching athletes private comment"):
            comment_response = self.client.get(constants.API_COMMENTS + str(self.private_comment.id) + "/")
            self.assertEqual(comment_response.status_code, 403)
        with self.subTest("Fetching athletes private workout-file"):
            file_response = self.client.get(constants.API_WORKOUT_FILES + str(self.private_file.id) + "/")
            self.assertEqual(file_response.status_code, 403)
    def test_athlete_non_private_workout(self):
        with self.subTest("Fetching athletes non-private workout"):
            workout_response = self.client.get(constants.API_WORKOUTS + str(self.athlete_non_private_workout.id) + "/")
            self.assertEqual(workout_response.status_code, 200)
        with self.subTest("Fetching athletes non-private comment"):
            comment_response = self.client.get(constants.API_COMMENTS + str(self.non_private_comment.id) + "/")
            self.assertEqual(comment_response.status_code, 200)
        with self.subTest("Fetching athletes non-private workout-file"):
            file_response = self.client.get(constants.API_WORKOUT_FILES + str(self.non_private_file.id) + "/")
            self.assertEqual(file_response.status_code, 200)
    def test_public_workout(self):
        with self.subTest("Fetching public workout"):
            workout_response = self.client.get(constants.API_WORKOUTS + str(self.public_workout.id) + "/")
            self.assertEqual(workout_response.status_code, 200)
        with self.subTest("Fetching public comment"):
            comment_response = self.client.get(constants.API_COMMENTS + str(self.public_comment.id) + "/")
            self.assertEqual(comment_response.status_code, 200)
        with self.subTest("Fetching public workout-file"):
            file_response = self.client.get(constants.API_WORKOUT_FILES + str(self.public_file.id) + "/")
            self.assertEqual(file_response.status_code, 200)