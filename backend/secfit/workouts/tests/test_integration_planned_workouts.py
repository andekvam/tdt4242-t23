from django.test import TestCase, Client, client
from django.urls import reverse
from rest_framework.test import APIRequestFactory, APITestCase
from workouts.models import Workout, WorkoutFile, Exercise, ExerciseInstance
from django.contrib.auth import get_user_model
from datetime import datetime
from django.core.exceptions import ValidationError
import json
from secfit import constants
"""
Integration
"""

class TestPlannedWorkoutImplementation(TestCase):
    def setUp(self):
        self.factory = APIRequestFactory()
        self.client = Client()
        self.user = get_user_model().objects.create(username="test")
        self.user2 = get_user_model().objects.create(username="test2")
        self.user.set_password("testpassword")
        self.user.save()

        self.workout = Workout.objects.create(name="workout1", owner=self.user, date="2021-03-18T15:00:00Z", planned="True", done="True")
        self.workout2 = Workout.objects.create(name="workout2", owner=self.user, date="2021-03-13T15:00:00Z", planned="False", done="False")

        self.exercise = Exercise.objects.create(name="exercise", description="no", unit="kg", exercise_type="CA")

        response = self.client.post("/api/token/", {"username": "test", "password": "testpassword"})
        self.client.defaults["HTTP_AUTHORIZATION"] = "Bearer " + json.loads(response.content)["access"]
    
    #Unit-testing the integration and implementation of planned workouts
    #Testing that planned is set True when it should
    def test_is_planned(self):
        response = self.client.get(constants.API_WORKOUTS + str(self.workout.id) + "/")
        with self.subTest():
            self.assertEqual(response.status_code, 200)
        with self.subTest():
            content = json.loads(response.content)
            self.assertTrue(content['planned'])

    #Testing that Done is set to True when it should be   
    def test_is_done(self):
        response = self.client.get(constants.API_WORKOUTS + str(self.workout.id) + "/")
        with self.subTest():
            self.assertEqual(response.status_code, 200)
        with self.subTest():
            content = json.loads(response.content)
            self.assertTrue(content['done'])
    
    #Testing that planned is set to False when it should be
    def test_is_not_planned(self):
        response = self.client.get(constants.API_WORKOUTS + str(self.workout2.id) + "/")
        with self.subTest():
            self.assertEqual(response.status_code, 200)
        with self.subTest():
            content = json.loads(response.content)
            self.assertFalse(content['planned'])

    #Testing that done is set to false when it should be
    def test_is_not_done(self):
        response = self.client.get(constants.API_WORKOUTS + str(self.workout2.id) + "/")
        with self.subTest():
            self.assertEqual(response.status_code, 200)
        with self.subTest():
            content = json.loads(response.content)
            self.assertFalse(content['done'])

    #Test that planned variable is set to True upon creation with correct API-endpoint   
    def test_creation_of_planned_workouts(self):
        body = {"name": "Workout test", "owner" : 1, "date": "2021-03-08T15:00:00Z","notes": "Test","visibility": "PU","workout_type": "ST","exercise_instances": [], "planned":True}
        response = self.client.post("/api/workouts/planned/", body, content_type="application/json")
        #Test status code equal 201 created
        with self.subTest():
            self.assertEqual(response.status_code, 201)
        #Test that upon creation, planned is correctly set to true
        with self.subTest():
            content = json.loads(response.content.decode())
            self.assertTrue(content['planned'])
            #Test that done is set to true after update request sent
            self.assertFalse(content['done'])
            response2 = self.client.put(constants.API_WORKOUTS +str(content['id'])+"/", {"name": "Workout test", "owner": 1, "date": "2021-03-08T15:00:00Z", "notes": "Test", "visibility": "PU", "workout_type": "ST", "exercise_instances": [], "done":True}, content_type="application/json")
            content2 = json.loads(response2.content.decode())
            self.assertTrue(content2['done'])
        
