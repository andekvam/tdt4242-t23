# Generated by Django 3.1 on 2021-02-25 11:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('workouts', '0003_rememberme'),
    ]

    operations = [
        migrations.AddField(
            model_name='workout',
            name='planned',
            field=models.BooleanField(default=False),
        ),
    ]
