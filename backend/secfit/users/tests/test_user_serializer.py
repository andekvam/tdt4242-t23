from rest_framework import serializers
from django.test import TestCase
from django.contrib.auth import get_user_model
from users.serializers import UserSerializer

class UserSerializerTestCase(TestCase):


    def test_validate_password(self):
        test_data = {
            "password":"hi",
            "password1":"hi",
        }
        test_serializer = UserSerializer(data=test_data)
        self.assertEqual(test_serializer.validate_password(True), True)
        
        #Test for correct error raised when passwords don't match
        test_data2 = {
            "password":"hi",
            "password":"wrong",
        } 
        with self.assertRaises(serializers.ValidationError):
            test_serializer2 = UserSerializer(data=test_data2)
            test_serializer2.validate_password(True)

    def test_create(self):
        test_serializer = UserSerializer()
        test_data = {
            "username": "test",
            "password": "hi",
            "email": "test@mail.no",
            "phone_number": "8",
            "country": "Norge",
            "city": "Trondheim",
            "street_address": "Glos"
        }
        test_user = test_serializer.create(test_data)
        check_user = get_user_model()(username="test", email="test@mail.no", phone_number="8", country="Norge", city="Trondheim", street_address="Glos")
        self.assertEqual(test_user.username, check_user.username)

# Create your tests here.
