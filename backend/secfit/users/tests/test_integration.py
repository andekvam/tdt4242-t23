import json
from django.test import TestCase, Client
from users.models import User, Offer
from secfit import constants
from django.forms.models import model_to_dict

class LoginIntegration(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create(username="user")
        self.user.set_password("test")
        self.user.save()
        self.validLoginBody = {"username":"user", "password":"test"}
        self.invalidLoginBody = {"username":"user", "password":"wrongpassword"}

    def test_login(self):
        with self.subTest("Login succeeds with correct credentials"):
            response = self.client.post("/api/token/", self.validLoginBody)
            self.assertEqual(response.status_code, 200)
        
        with self.subTest("Successful login returns access and refresh tokens"):
            self.assertIn("access", json.loads(response.content.decode()))
            self.assertIn("refresh", json.loads(response.content.decode()))

        with self.subTest("Login fails with incorrect credentials"):
            response = self.client.post("/api/token/", self.invalidLoginBody)
            self.assertEqual(response.status_code, 401)
    
class OfferIntegration(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create(username="user")
        self.user.set_password("test")
        self.user.save()
        response = self.client.post("/api/token/", {"username": "user", "password": "test"})
        self.client.defaults["HTTP_AUTHORIZATION"] = "Bearer " + json.loads(response.content)["access"]
        self.coach = User.objects.create(username="coach", password="test")

        self.offer = Offer.objects.create(owner=self.coach, recipient=self.user, status="p", timestamp=constants.DATE)

    def test_fetch_offer(self):
        response = self.client.get(constants.API_OFFERS)
        with self.subTest("Endpoint returns 200 OK"):
            self.assertEqual(response.status_code, 200)
        with self.subTest("Endpoint returns offers"):
            response_dict = json.loads(response.content.decode())
            self.assertEqual(int(response_dict["count"]), 1)
        with self.subTest("Offer matches database entry on key values"):
            offer = model_to_dict(Offer.objects.get(pk=1))
            offer_coach = User.objects.get(pk=offer["owner"])
            offer_response = response_dict["results"][0]
            # Rewrite offer recipient to match format of database entry
            offer_response["recipient"] = int(offer_response["recipient"].split("/")[-2])

            self.assertEqual(offer_response["id"], offer["id"])
            self.assertEqual(offer_response["status"], offer["status"])
            self.assertEqual(offer_response["recipient"], offer["recipient"])
            self.assertEqual(offer_response["owner"], offer_coach.username)
        
    def test_patch_offer(self):
        body = { "status": "a"}
        response = self.client.patch(f"{constants.API_OFFERS}{self.offer.id}/", body, content_type=constants.CONTENT_TYPE)
        with self.subTest("Endpoint returns 200 OK"):
            self.assertEqual(response.status_code, 200)

        with self.subTest("Change made to offer is reflected when fetched again"):
            response = self.client.get(f"{constants.API_OFFERS}{self.offer.id}/")
            response_dict = json.loads(response.content.decode())
            self.assertEqual(response_dict["status"], "a")
        
    def test_delete_offer(self):
        with self.subTest("User can delete offer"):
            response = self.client.delete(f"{constants.API_OFFERS}{self.offer.id}/")
            self.assertEqual(response.status_code, 204)
        
        with self.subTest("Offer can not be fetched after deletion"):
            response = self.client.get(f"{constants.API_OFFERS}{self.offer.id}/")
            self.assertEqual(response.status_code, 404)