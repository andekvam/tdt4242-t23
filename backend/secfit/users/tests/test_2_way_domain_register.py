from django.test import TestCase
from faker import Faker
from factory.fuzzy import FuzzyText
import string
from allpairspy import AllPairs
from collections import OrderedDict
from unittest import skip
from ..forms import CustomUserCreationForm

class TestRegisterPage(TestCase):
    def setUp(self):
        fake = Faker() # Generate fake data using a faker generator

        # Configure values used in the success test case
        self.approved_username = FuzzyText(length=20).fuzz()
        self.approved_email_1 = fake.email()
        self.approved_password_1 = fake.password()
        self.approved_password_2 = fake.password()
        self.approved_textfield_50 = FuzzyText(length=40).fuzz()
        
        # Configure declined parameters for use in the test cases
        username_1 = ""
        username_2 = FuzzyText(length=50, chars=string.punctuation).fuzz()
        username_3 = FuzzyText(length=151).fuzz()
        email_1 = FuzzyText(length=245, suffix="@gmail.com").fuzz()
        email_2 = FuzzyText(length=20).fuzz()
        password_1 = FuzzyText(length=4).fuzz()
        password_2 = FuzzyText(length=16, chars=string.digits).fuzz()
        textfield_50_1 = ""
        textfield_50_2 = FuzzyText(length=60).fuzz()

        declined_parameters = [
            [username_1, username_2, username_3],
            [email_1, email_2],
            [password_1, password_2], 
            [textfield_50_1, textfield_50_2]
        ]

        # Generate all possible combinations
        self.declined_combinations = list(AllPairs(declined_parameters))

        self.declined_parameters_dict = OrderedDict({
            "username": [username_1, username_2, username_3],
            "email": [email_1, email_2],
            "password": [password_1, password_2], 
            "textfield_50": [textfield_50_1, textfield_50_2]
            
        })
        
    # All combinations
    def test_declined_combinations(self):
        for _, pairs in enumerate(AllPairs(self.declined_parameters_dict)):
            data = {
                'username': pairs.username,
                'email': pairs.email,
                'password1': pairs.password,
                'password2': pairs.password,
                'phone_number': pairs.textfield_50,
                'country': pairs.textfield_50,
                'city': pairs.textfield_50,
                'street_address': pairs.textfield_50
            }
            form = CustomUserCreationForm(data)

            # Using subtest to prevent return of test failure immediately,
            # possibly before all combinations are tested!
            with self.subTest(form=form):
                self.assertFalse(form.is_valid())
    
    # Special test case when passwords are not equal
    def test_different_passwords(self):
        data = {
            'username': self.approved_username ,
            'email': self.approved_email_1,
            'password1': self.approved_password_1,
            'password2': self.approved_password_2,
            'phone_number': self.approved_textfield_50,
            'country': self.approved_textfield_50,
            'city': self.approved_textfield_50,
            'street_address': self.approved_textfield_50    
        }
        form = CustomUserCreationForm(data)
        self.assertFalse(form.is_valid())
    
    # Test case when form is valid 
    def test_valid_form(self):
        data = {
            'username': self.approved_username ,
            'email': self.approved_email_1,
            'password1': self.approved_password_1,
            'password2': self.approved_password_1,
            'phone_number': self.approved_textfield_50,
            'country': self.approved_textfield_50,
            'city': self.approved_textfield_50,
            'street_address': self.approved_textfield_50  
        }
        form = CustomUserCreationForm(data)
        self.assertTrue(form.is_valid())


    





