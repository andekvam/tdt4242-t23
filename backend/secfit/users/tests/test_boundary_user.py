from django.test import TestCase, Client
from secfit import constants

# Create your tests here.
class UserRegistrationBoundaryTest(TestCase):

    def setUp(self):
        self.client = Client()
        self.data = {
            "username":"aaa",
            "email": "aaa@mail.com",
            "password": "Password",
            "password1": "Password",
            "phone_number": "99999999",
            "country": "Norway",
            "city": "Oslo",
            "street_address": "Slottsplassen"
        }
    
    def test_normal_user_registration(self):
        response = self.client.post(constants.API_USERS, self.data)
        self.assertEqual(response.status_code, 201)

    def test_username(self):
        # Empty username, should fail
        self.data["username"] = ""
        response = self.client.post(constants.API_USERS, self.data)
        self.assertEqual(response.status_code, 400)

        # Min length username, should pass
        self.data["username"] = "a"
        response = self.client.post(constants.API_USERS, self.data)
        self.assertEqual(response.status_code, 201)

        # Max length username 150 characters, should pass
        self.data["username"] = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
        response = self.client.post(constants.API_USERS, self.data)
        self.assertEqual(response.status_code, 201)

        # Max length username + 1, should fail 
        self.data["username"] = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
        response = self.client.post(constants.API_USERS, self.data)
        self.assertEqual(response.status_code, 400) 

    def test_email(self):
        # Empty email, should pass since it's not a required field for now
        self.data["email"] = ""
        response = self.client.post(constants.API_USERS, self.data)
        self.assertEqual(response.status_code, 201)

        # Min length email with a new username as it has to be unique per user, should pass
        self.data["username"] = "b"
        self.data["email"] = "a@a.no"
        response = self.client.post(constants.API_USERS, self.data)
        self.assertEqual(response.status_code, 201)

        # No upper boundary on length, testing invalid email instead
        self.data["username"] = "c"
        self.data["email"] = "a.a.no"
        response = self.client.post(constants.API_USERS, self.data)
        self.assertEqual(response.status_code, 400)

    def test_password(self):
        #Empty password, should fail
        self.data["password"] = ""
        self.data["password1"] = ""
        response = self.client.post(constants.API_USERS, self.data)
        self.assertEqual(response.status_code, 400)

        #Min length password, should pass
        self.data["password"] = "a"
        self.data["password1"] = "a"
        response = self.client.post(constants.API_USERS, self.data)
        self.assertEqual(response.status_code, 201)

        # No upper boundary on password length, testing different repeat password instead. Should fail
        self.data["username"] = "c"
        self.data["password"] = "a"
        self.data["password1"] = "ab"
        response = self.client.post(constants.API_USERS, self.data)
        self.assertEqual(response.status_code, 400)

    def test_phone_number(self):
        # Empty phone number, should pass as it is not a required field
        self.data["phone_number"] = ""
        response = self.client.post(constants.API_USERS, self.data)
        self.assertEqual(response.status_code, 201)

        # Min length phone number, should pass for now as there is no validation.
        self.data["username"] = "b"
        self.data["phone_number"] = "1"
        response = self.client.post(constants.API_USERS, self.data)
        self.assertEqual(response.status_code, 201)

        # Max length phone number, should pass for now as there is no validation.
        self.data["username"] = "c"
        self.data["phone_number"] = "99999999999999999999999999999999999999999999999999"
        response = self.client.post(constants.API_USERS, self.data)
        self.assertEqual(response.status_code, 201)

        # Max length + 1 phone number, should fail
        self.data["username"] = "d"
        self.data["phone_number"] = "999999999999999999999999999999999999999999999999999"
        response = self.client.post(constants.API_USERS, self.data)
        self.assertEqual(response.status_code, 400)
    
    def test_country(self):
        # Empty country, should pass as it is not a required field
        self.data["country"] = ""
        response = self.client.post(constants.API_USERS, self.data)
        self.assertEqual(response.status_code, 201)

        # Min length country, should pass
        self.data["username"] = "b"
        self.data["country"] = "A"
        response = self.client.post(constants.API_USERS, self.data)
        self.assertEqual(response.status_code, 201)

        # Max length country, should pass.
        self.data["username"] = "c"
        self.data["country"] = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
        response = self.client.post(constants.API_USERS, self.data)
        self.assertEqual(response.status_code, 201)

        # Max length + 1 country, should fail
        self.data["username"] = "d"
        self.data["country"] = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
        response = self.client.post(constants.API_USERS, self.data)
        self.assertEqual(response.status_code, 400)
    
    def test_city(self):
        # Empty city, should pass as it is not a required field
        self.data["city"] = ""
        response = self.client.post(constants.API_USERS, self.data)
        self.assertEqual(response.status_code, 201)

        # Min length city, should pass
        self.data["username"] = "b"
        self.data["city"] = "A"
        response = self.client.post(constants.API_USERS, self.data)
        self.assertEqual(response.status_code, 201)

        # Max length city, should pass
        self.data["username"] = "c"
        self.data["city"] = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
        response = self.client.post(constants.API_USERS, self.data)
        self.assertEqual(response.status_code, 201)

        # Max length + 1 city, should fail
        self.data["username"] = "d"
        self.data["city"] = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
        response = self.client.post(constants.API_USERS, self.data)
        self.assertEqual(response.status_code, 400)

    def test_street_address(self):
        # Empty street address, should pass as it is not a required field
        self.data["street_address"] = ""
        response = self.client.post(constants.API_USERS, self.data)
        self.assertEqual(response.status_code, 201)

        # Min length street address, should pass.
        self.data["username"] = "b"
        self.data["street_addressr"] = "A"
        response = self.client.post(constants.API_USERS, self.data)
        self.assertEqual(response.status_code, 201)

        # Max length street address, should pass
        self.data["username"] = "c"
        self.data["street_address"] = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
        response = self.client.post(constants.API_USERS, self.data)
        self.assertEqual(response.status_code, 201)

        # Max length + 1 street address, should fail
        self.data["username"] = "d"
        self.data["street_address"] = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
        response = self.client.post(constants.API_USERS, self.data)
        self.assertEqual(response.status_code, 400)